package com.bortnak.bsc.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Custom exception for not valid input
 *
 * @author Tomas Bortnak
 */
@AllArgsConstructor
@Getter
public class InvalidInputException extends Exception {

    private final String exceptionMessage;
}
