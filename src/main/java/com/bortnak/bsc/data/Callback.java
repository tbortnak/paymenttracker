package com.bortnak.bsc.data;

/**
 * Use callback when time is elapsed
 *
 * @author Tomas Bortnak
 */
public interface Callback {
    void onTimeElapsed();
}
