package com.bortnak.bsc.data;

import com.bortnak.bsc.common.Utils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Payment is a main entity which is used for payment operations in application
 *
 * @author Tomas Bortnak
 */
@NoArgsConstructor
@Getter
@ToString
public class Payment {

    private String currency;
    private Double amount;
    private Double inDollars;


    public Payment(String currency, Double amount) {
        this.currency = currency;
        this.amount = amount;
        this.inDollars = this.amount / Utils.getConversionRate(currency);
    }


    /**
     * setter currency Amount which recalculate amount to USD
     *
     * @param currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * setter for Amount which recalculate amount to USD
     *
     * @param amount
     */
    public void setAmount(Double amount) {
        this.amount = amount;
        this.inDollars = this.amount / Utils.getConversionRate(currency);
    }
}
