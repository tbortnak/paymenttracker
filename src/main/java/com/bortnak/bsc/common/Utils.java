package com.bortnak.bsc.common;

/**
 * Utils is a class in commom package, which contains common utilities
 *
 * @author Tomas Bortnak
 */
public class Utils {

    private static final String EUR = "EUR";
    private static final String USD = "USD";
    private static final String GBP = "GBP";
    private static final String HKD = "HKD";
    private static final String CZK = "CZK";
    private static final String HUN = "HUN";
    private static final String CHF = "CHF";
    private static final String BHD = "BHD";

    public static final String[] SUPPORTED_CURRENCY = new String[]{
            EUR,
            USD,
            GBP,
            HKD,
            CZK,
            HUN,
            CHF,
            BHD
    };


    /**
     * Method return conversion rate from currency to USD
     *
     * @param currency
     * @return exchange rate to USD
     */
    public static double getConversionRate(String currency) {
        switch (currency) {
            case EUR:
                return 0.89736;
            case USD:
                return 1;
            case GBP:
                return 0.80189;
            case HKD:
                return 7.81293;
            case CZK:
                return 22.9080;
            case HUN:
                return 292.105;
            case CHF:
                return 0.98553;
            case BHD:
                return 0.37456;
            default:
                return 1;
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
