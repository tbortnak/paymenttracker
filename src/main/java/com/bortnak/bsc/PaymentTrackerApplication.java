package com.bortnak.bsc;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * PaymentTrackerApplication is Main class of application
 *
 * @author Tomas Bortnak
 */
@SpringBootApplication
public class PaymentTrackerApplication implements CommandLineRunner {

    private final ConsoleRunner consoleRunner;

    public PaymentTrackerApplication(ConsoleRunner consoleRunner) {
        this.consoleRunner = consoleRunner;
    }

    /**
     * main method of application
     */
    public static void main(String[] args) {
        SpringApplication.run(PaymentTrackerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        consoleRunner.run();
    }
}
