package com.bortnak.bsc;

import com.bortnak.bsc.common.Utils;
import com.bortnak.bsc.data.Callback;
import com.bortnak.bsc.data.Payment;
import com.bortnak.bsc.exception.InvalidInputException;
import com.bortnak.bsc.file.FileReader;
import com.bortnak.bsc.service.CurrencyService;
import com.bortnak.bsc.service.InputReader;
import com.bortnak.bsc.service.TimerService;
import com.bortnak.bsc.service.TimerServiceImpl;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * ConsoleRunner is implementation for console part of application
 *
 * @author Tomas Bortnak
 */
@Component
public class ConsoleRunner {

    private final TimerService timerService;
    private final InputReader inputReader;
    private final CurrencyService currencyService;
    private final FileReader fileReader;

    public ConsoleRunner(TimerServiceImpl timerService, InputReader inputReader, CurrencyService currencyService, FileReader fileReader) {
        this.timerService = timerService;
        this.inputReader = inputReader;
        this.currencyService = currencyService;
        this.fileReader = fileReader;
    }

    /**
     * Contans application runner
     */
    public void run() {
        int select;
        System.out.println("|----------------------------------------------------------------|");
        System.out.println("|                       PAYMENT TRACKER                          |");
        System.out.println("| Options:                                                       |");
        System.out.println("|        1 Load input from file and start payment tracker        |");
        System.out.println("|        2 Start payment tracker without file loading            |");
        System.out.println("|        3 Exit                                                  |");
        System.out.println("|________________________________________________________________|");
        System.out.println(" Please choose your option: ");
        Scanner scanner = new Scanner(System.in);
        select = scanner.nextInt();

        // Switch construct
        switch (select) {
            case 1:
                System.out.println("Option 1 selected");
                List<Payment> paymentsListFromExcel = fileReader.getPaymentsListFromExcel();
                paymentsListFromExcel.stream().forEach(currencyService::addNewEntry);
                timerService.startTimerTask();
                timerService.onTimerElapsed(callback);
                runPayments();
                break;
            case 2:
                System.out.println("Option 2 selected");
                timerService.startTimerTask();
                timerService.onTimerElapsed(callback);
                runPayments();
                break;
            case 3:
                System.out.println("Exit selected");
                System.exit(0);
                break;
            default:
                System.out.println("Invalid selection");
                break; // This break is not really necessary
        }


//        timerService.stopTimerTask();
    }

    /**
     * Method which enable listening from new payment input
     */
    private void runPayments() {
        while (true) {
            inputReader.startListeningForConsoleInput();
            try {
                Payment payment = inputReader.getConsoleInput();
                if (payment != null) {
                    currencyService.addNewEntry(payment);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InvalidInputException e) {
                System.out.println(e.getExceptionMessage());
            }
        }
    }

    private Callback callback = new Callback() {
        @Override
        public void onTimeElapsed() {
            StringBuilder stringBuilder = new StringBuilder();
            currencyService.getPaymentsList().stream().forEach(payment -> {
                stringBuilder.append(payment.getCurrency());
                stringBuilder.append(" ");
                stringBuilder.append(payment.getAmount());

                if (Arrays.asList(Utils.SUPPORTED_CURRENCY).contains(payment.getCurrency())) {
                    stringBuilder.append("(" + Utils.round(payment.getInDollars(), 2) + " USD)");
                } else {
                    stringBuilder.append("( --- )");
                }

                stringBuilder.append("\n");
            });

            System.out.println(stringBuilder.toString());
        }
    };
}
