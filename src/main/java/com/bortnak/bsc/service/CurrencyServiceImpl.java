package com.bortnak.bsc.service;

import com.bortnak.bsc.data.Payment;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * CurrencyServiceImpl is service class for Currency operations
 *
 * @author Tomas Bortnak
 */
@Service
public class CurrencyServiceImpl implements CurrencyService {

    private LinkedHashMap<String, Double> payments = new LinkedHashMap<>();

    /**
     * Method for add new payment from input
     *
     * @param payment
     */
    @Override
    public void addNewEntry(Payment payment) {
        calculatePayment(payment);
    }

    /**
     * Method which returns list of current status of payments
     *
     * @return result (List<Payment>)
     */
    @Override
    public List<Payment> getPaymentsList() {
        List<Payment> result = payments.entrySet().stream().map(entry -> new Payment(entry.getKey(), entry.getValue())).collect(Collectors.toList());
        return result;
    }

    /**
     * method check if currency exist and recalculate amount for current currency
     *
     * @param payment
     */
    private void calculatePayment(Payment payment) {
        List<Double> collect = payments.entrySet().stream().filter(entry -> entry.getKey().equals(payment.getCurrency())).map(entry -> entry.setValue(entry.getValue() + payment.getAmount())).collect(Collectors.toList());
        if (collect.isEmpty()) {
            payments.put(payment.getCurrency(), payment.getAmount());
        }
        payments.entrySet().removeIf(entry -> entry.getValue() == 0);
    }
}
