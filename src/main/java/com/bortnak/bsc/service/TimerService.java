package com.bortnak.bsc.service;

import com.bortnak.bsc.data.Callback;

/**
 * This is interface fo TimerService
 *
 * @author Tomas Bortnak
 */
public interface TimerService {

    void startTimerTask();

    void stopTimerTask();

    void onTimerElapsed(Callback callback);
}
