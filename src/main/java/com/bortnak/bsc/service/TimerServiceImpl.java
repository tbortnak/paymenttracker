package com.bortnak.bsc.service;

import com.bortnak.bsc.data.Callback;
import org.springframework.stereotype.Service;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * This class is implementation of service which handle timer in application
 *
 * @author Tomas Bortnak
 */
@Service
public class TimerServiceImpl implements TimerService {

    private final static String CONSOLE_TIMER = "CONSOLE_TIMER";
    private final static Long REPEAT_TIME = TimeUnit.MINUTES.toMillis(1);

    private Timer timer;
    private Callback callback;

    public TimerServiceImpl() {
    }

    /**
     * Method which enable countdown
     */
    @Override
    public void startTimerTask() {
        timer = new Timer(CONSOLE_TIMER);
        timer.scheduleAtFixedRate(timerTask, REPEAT_TIME, REPEAT_TIME);
    }

    /**
     * Method which disable countdown
     */
    @Override
    public void stopTimerTask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    /**
     * Method which notify when countdown elapsed
     *
     * @param callback
     */
    @Override
    public void onTimerElapsed(Callback callback) {
        this.callback = callback;
    }

    private TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            if (callback != null) {
                callback.onTimeElapsed();
            }
        }
    };
}
