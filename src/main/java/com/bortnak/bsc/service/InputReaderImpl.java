package com.bortnak.bsc.service;

import com.bortnak.bsc.data.Payment;
import com.bortnak.bsc.exception.InvalidInputException;
import com.bortnak.bsc.file.FileWriter;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * ImputReaderImpl is class which remain from methods which are used for imput handling
 *
 * @author Tomas Bortnak
 */
@Service
public class InputReaderImpl implements InputReader {

    private static final String SEPARATOR = " ";
    private static final String AMOUNT_REGEX = "-?[0-9]+";
    private static final String CURRENCY_REGEX = "[A-Z]{3}";

    private final FileWriter fileWriter;
    private final CurrencyService currencyService;

    private BufferedReader bufferedReader;

    public InputReaderImpl(FileWriter fileWriter, CurrencyService currencyService) {
        this.fileWriter = fileWriter;
        this.currencyService = currencyService;
    }

    /**
     * Method which is used for open input stream from console
     */
    @Override
    public void startListeningForConsoleInput() {
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * Method which is used for close input stream from console
     */
    @Override
    public void stopListeningForConsoleInput() throws IOException {
        bufferedReader.close();
        bufferedReader = null;

    }

    /**
     * Method which is used reaing current input from console
     *
     * @return Payment(currency, ammount)
     */
    @Override
    public Payment getConsoleInput() throws IOException, InvalidInputException {
        String line = bufferedReader.readLine();
        String[] tokens = line.split(SEPARATOR);
        if (tokens.length == 1 && line.toString().equals("quit")) {
            fileWriter.writePaymentsListToExcel(currencyService.getPaymentsList());
            stopListeningForConsoleInput();
            System.exit(0);
        }
        if (tokens.length == 0 || tokens.length > 2) {
            throw new InvalidInputException("Not valid input format. Example: \"USD 1000\"");
        } else {
            String currency = getCurrency(tokens[0]);
            Double amount = getAmount(tokens[1]);
            if (currency != null && amount != null) {
                return new Payment(currency, amount);
            } else {
                return null;
            }
        }
    }

    /**
     * Method which is used for getting ammount from payment
     *
     * @param input
     */
    private Double getAmount(String input) throws InvalidInputException {
        if (input.matches(AMOUNT_REGEX)) {
            return Double.valueOf(input);
        } else {
            throw new InvalidInputException("Not valid input format. Example: \"USD 1000\"");
        }

    }

    /**
     * Method which is used for getting currency from payment
     *
     * @param input
     */
    private String getCurrency(String input) throws InvalidInputException {
        if (input.matches(CURRENCY_REGEX)) {
            return input;
        } else {
            throw new InvalidInputException("Not valid input format. Example: \"USD 1000\"");
        }
    }
}
