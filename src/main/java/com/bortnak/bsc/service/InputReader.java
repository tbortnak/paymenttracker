package com.bortnak.bsc.service;

import com.bortnak.bsc.data.Payment;
import com.bortnak.bsc.exception.InvalidInputException;

import java.io.IOException;

/**
 * This is interface for InputReader
 *
 * @author Tomas Bortnak
 */
public interface InputReader {

    void startListeningForConsoleInput();

    void stopListeningForConsoleInput() throws IOException;

    Payment getConsoleInput() throws IOException, InvalidInputException;
}
