package com.bortnak.bsc.service;

import com.bortnak.bsc.data.Payment;

import java.util.List;

/**
 * This is interface for CurrencyService
 *
 * @author Tomas Bortnak
 */
public interface CurrencyService {

    void addNewEntry(Payment payment);

    List<Payment> getPaymentsList();
}
