package com.bortnak.bsc.file;

import com.bortnak.bsc.data.Payment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * FileWriter is class in file package which is used for insert output to excel file
 *
 * @author Tomas Bortnak
 */
@Component
public class FileWriter {

    private static final String FILE_PATH = "output.xlsx";
    //We are making use of a single instance to prevent multiple write access to same file.
    private static final FileWriter INSTANCE = new FileWriter();

    public FileWriter() {
    }

    /**
     * Method for writing output to excel file
     *
     * @param List<Payment> paymentList
     */
    public static void writePaymentsListToExcel(List<Payment> paymentList) {

        // Using XSSF for xlsx format, for xls use HSSF
        Workbook workbook = new XSSFWorkbook();

        Sheet paymentsSheet = workbook.createSheet("Payments");

        int rowIndex = 0;
        for (Payment payment : paymentList) {
            Row row = paymentsSheet.createRow(rowIndex++);
            int cellIndex = 0;
            //first place in row is name
            row.createCell(cellIndex++).setCellValue(payment.getCurrency());

            //second place in row is marks in maths
            row.createCell(cellIndex++).setCellValue(payment.getAmount());

        }

        //write this workbook in excel file.
        try {
            FileOutputStream fos = new FileOutputStream(FILE_PATH);
            workbook.write(fos);
            fos.close();

            System.out.println(FILE_PATH + " is successfully written");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
