package com.bortnak.bsc;

import com.bortnak.bsc.data.Payment;
import com.bortnak.bsc.service.CurrencyServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

/**
 * Test class for CurrentlyServiceImpl
 *
 * @author Tomas Bortnak
 */
@RunWith(MockitoJUnitRunner.class)
public class CurrencyServiceImplTest {

    @InjectMocks
    CurrencyServiceImpl currencyService;

    @Test
    public void addNewEntry_valueNotPresent() {
        currencyService.addNewEntry(new Payment("USD", 100.0));
        currencyService.addNewEntry(new Payment("EUR", 100.0));

        assertEquals(2, currencyService.getPaymentsList().size());
    }

    @Test
    public void addNewEntry_valuePresent() {
        currencyService.addNewEntry(new Payment("USD", 100.0));
        currencyService.addNewEntry(new Payment("USD", 100.0));

        assertEquals(1, currencyService.getPaymentsList().size());
    }
}
