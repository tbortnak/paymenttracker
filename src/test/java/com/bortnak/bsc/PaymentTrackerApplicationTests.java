package com.bortnak.bsc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test class for main class of application
 *
 * @author Tomas Bortnak
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentTrackerApplicationTests {

    @Test
    public void contextLoads() {
    }

}
